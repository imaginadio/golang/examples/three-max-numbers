package main

import (
	"fmt"
)

func main() {
	input := []int{5, 4, 3, 1, 5}
	var max1, max2, max3 int
	for _, v := range input {
		if v > max1 {
			max3 = max2
			max2 = max1
			max1 = v
		} else if v > max2 {
			max3 = max2
			max2 = v
		} else if v > max3 {
			max3 = v
		}

	}

	fmt.Println(max1, max2, max3)

}
